//CONSTANTS
const searchedPassword = "824f7d4bc3707f5d88e4cd885cae1cc8e3ac172d";

//UTILS FUNCTION
function start_scroll_down() { 
    scroll = setInterval(function(){ window.scrollBy(0, 1000); console.log('start');}, 800);
 }
 
 function stop_scroll_down() {
    clearInterval(scroll);
    console.log('stop');
 }

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

let stringFromArray = (arr) => (
    arr.reduce((a, b) => a.concat(b), "")
)

let searchCases = (character) => {
    switch(character.toLowerCase()){
        case 'e':
            return '3';
        case 'a':
            return '4';
        case 'i' :
            return '1';
        case 's' :
            return '5';
        case 'g' :
            return '9';
        case 't' :
            return '7';
        case 'o':
            return '0';
        default :
            return character;
    }
}

const changeWordFormat = (word) => (
    word.toLowerCase().replace(/^./, word[0].toUpperCase())
)

const replaceCharactersToNumbers = (word) => (
    [...word].map((letter) => searchCases(letter)).join("")
)

const hashPassword = (password) => (
    sha1(password)
)

const matchPassword = (password) => {
    let newHash = hashPassword(password);
    return newHash.valueOf() == searchedPassword.valueOf();
}

// CORE FUNCTIONS

const makePermutations = (args) => (
    Combinatorics.permutation(args).toArray().map((arr) => stringFromArray(arr))
)

const changeWordListFormatWithNumbers = (wordList) => (
    wordList.map((word) => changeWordFormat(replaceCharactersToNumbers(word.trim())))
)

const changeWordListFormat = (wordList) => (
    wordList.map((word) => changeWordFormat(word.trim()))
)


async function findPassword  (words, t2) {
    start_scroll_down();
    let bool = false;
    let correcta = "";
    t2.print('Comprobando las contraseñas......');
    let arr = makePermutations(changeWordListFormat(words));
    arr.push(...makePermutations(changeWordListFormatWithNumbers(words)));
    for (let i=0; i < arr.length; i++){
        t2.print("Probando contraseña "+ arr[i] + " .......");
        t2.print(" * Se utilizó el hash secreto -->"+ hashPassword(arr[i]));
        await sleep(600);
        if(matchPassword(arr[i])){
            bool = true;
            t2.print("\n ** La contraseña " + arr[i] + " Es Correcta !!!!");
            correcta = arr[i];
            break;
        }else{
            t2.print("\nLa contraseña " + arr[i] + " no coincide con la almacenada !!!!");
        }
        t2.print("=================================================================");
        t2.print(".");
        t2.print(".");
        //await sleep(500);
    }
    let t3 = new Terminal('terminal_3');
    stop_scroll_down();
    t2.input('..... Presione enter para continuar ......', function(input) {
        t2.clear();
        document.getElementById('terminal_2').remove(); 
        document.body.appendChild(t3.html);
        t3.setTextSize("3em");
        if (bool){
            t3.setBackgroundColor('blue');
            t3.setHeight("100%");
            t3.setWidth('100%');
            document.body.style.background="blue";
            t3.print("Usted ha encontrado la contraseña correcta !!!");
            t3.print("* Puede ingresar al sistema utilizando la contraseña " + correcta);
        }else{
            t3.setBackgroundColor('red');
            t3.setHeight("100%");
            t3.setWidth('100%');
            document.body.style.background="red";
            t3.print("Intruso detectado !!!!!, activando las alarmas.");
        }
        
    });
}

const main = _ => {
    let arr = [];
    let t1 = new Terminal('terminal_1');
    let t2 = new Terminal('terminal_2');
    t1.setHeight("100%");
    t1.setWidth('100%');
    document.body.appendChild(t1.html);

    t1.print('Bienvenido señor bruno !');
    t1.print('Por favor ingrese las cuatro palabras clave para ingresar al sistema: ');
    
    t1.input('$ Palabra 1: ', function(input) {
        t1.print('\n')
        arr.push(input);
        t1.input('$ Palabra 2: ', function(input) {
            t1.print('\n')
            arr.push(input);
            t1.input('$ Palabra 3: ', function(input) {
                t1.print('\n')
                arr.push(input);
                t1.input('$ Palabra 4: ', function(input) {
                    t1.print('\n')
                    arr.push(input);
                    t1.print('=============================================================');
                    t1.input('..... Presione enter para continuar ......', function(input) {
                        t1.clear();
                        document.getElementById('terminal_1').remove(); 
                        t2.setBackgroundColor('green');
                        t2.setHeight("100%");
                        t2.setWidth('100%');
                        document.body.style.background="green";
                        document.body.appendChild(t2.html);
                        findPassword(arr,t2);
                    });
                })
            })
        })
    })
}

window.onload = main;